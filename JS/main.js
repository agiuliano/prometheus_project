"use strict";

var rootRef = firebase.database().ref().child("entries");

window.onload =function(){
    
    var traits = {};
    var needs = {};
    
    rootRef.on('child_added', snap =>{
        var data = JSON.parse(JSON.stringify(snap.child("Insights").val()));
        console.log(data);
        
        // Cheerfulness
        traits["Cheerfulness"] = data["personality"]["2"]["children"]["2"]["percentile"];
        
        // Prone to worry
        traits["Prone to Worry"] = data["personality"]["4"]["children"]["1"]["percentile"];
        
        // Susceptible to stress
        traits["Susceptible to Stress"] = data["personality"]["4"]["children"]["5"]["percentile"];
        
        // Self-consciousness
        traits["Self-Consciousness"] = data["personality"]["4"]["children"]["4"]["percentile"];
        
        // Melancholy
        traits["Melancholy"] = data["personality"]["4"]["children"]["2"]["percentile"];
        
        // Taking pleasure in life
        traits["Taking Pleasure in Life"] = data["values"]["4"]["percentile"];
        
        traits["Self-Enhancement"] = data["values"]["3"]["percentile"];
        
        // Needs
        // Harmony
        needs["Harmony"] = data["needs"]["4"]["percentile"];
        
        // Love
        needs["Love"] = data["needs"]["7"]["percentile"];
        
        // Closeness
        needs["Closeness"] = data["needs"]["1"]["percentile"];
        
        // Stability
        needs["Stability"] = data["needs"]["10"]["percentile"];
        
        // Excitement
        needs["Excitement"] = data["needs"]["3"]["percentile"];
        
        var items = Object.keys(traits).map(function(key){return [key, traits[key]]});
        items.sort(function(first, second) {return second[1]-first[1]});
        console.log(items);
        
        var rating = 0;
        var risk;
        rating += traits["Cheerfulness"];
        rating += (1-traits["Prone to Worry"]);
        rating += (1-traits["Susceptible to Stress"]);
        rating += (1-traits["Self-Consciousness"]);
        rating += (1-traits["Melancholy"]);
        rating += traits["Taking Pleasure in Life"];
        rating += traits["Self-Enhancement"];
        if (rating > 4) risk = 0;
        else if (rating > 2) risk = 1;
        else risk = 2;
        console.log(rating);
        
        var status = document.getElementsByClassName("status")[0];
        switch(risk){
            case 0:
                status.innerHTML = "Low Risk";
                status.id = "low";
                break;
            case 1:
                status.innerHTML = "Moderate Risk";
                status.id = "moderate";
                break;
            case 2:
                status.innerHTML = "High Risk";
                status.id = "high";
                break;
        }
        
        
        var ctx = document.getElementById("traitsChart").getContext("2d");
        var myChart = new Chart(ctx, {
            type: "bar",
            data: {
                labels: Object.keys(traits),
                datasets: [{
                    label: "Percentile",
                    data: Object.values(traits),
                    backgroundColor: [
                        setColor(traits['Cheerfulness'], 'Cheerfulness'),
                        setColor(traits['Prone to Worry'], 'Prone to Worry'),
                        setColor(traits['Susceptible to Stress'], 'Susceptible to Stress'),
                        setColor(traits['Self-Consciousness'], 'Self-Consciousness'),
                        setColor(traits['Melancholy'], 'Melancholy'),
                        setColor(traits['Taking Pleasure in Life'], 'Taking Pleasure in Life'),
                        setColor(traits['Self-Enhancement'], 'Self-Enhancement'),
                    ]
                }]
            }
        })
        var ctx2 = document.getElementById("needsChart").getContext("2d");
        var myChart2 = new Chart(ctx2, {
            type: "bar",
            data: {
                labels: Object.keys(needs),
                datasets: [{
                    label: "Percentile",
                    data: Object.values(needs),
                    backgroundColor: [
                        "rgb(0, 141, 149)",
                        "rgb(96, 39, 107)",
                        "rgb(0, 141, 149)",
                        "rgb(96, 39, 107)",
                        "rgb(0, 141, 149)",
                        "rgb(96, 39, 107)",
                        "rgb(0, 141, 149)",
                    ]
                }]
            }
        })
    });
    
    
    
    
    var name = "This person";
    var status_header = document.getElementById("status_header");
    status_header.innerHTML = name + " is determined to be at...";
}

function setColor(percentile, category) {
    var goodCategories = ['Cheerfulness', 'Taking Pleasure in Life', 'Self-Enhancement'];
    if (goodCategories.includes(category)){
        if (percentile > 0.66){
            return 'rgb(0, 254, 102)';
        }
        else if(percentile > 0.33){
            return 'rgb(240, 255, 78)';
        }
        else{
            return 'rgb(220, 62, 0)';
        }
    }
    else {
        if (percentile < 0.33){
            return 'rgb(0, 254, 102)';
        }
        else if(percentile < 0.66){
            return 'rgb(240, 255, 78)';
        }
        else{
            return 'rgb(220, 62, 0)';
        }
    }
}



